GradientRenderer = function (netMap) {

  var neurons = netMap.neurons;
  var connections = netMap.connections;

  this.draw = function ()
  {
    this.drawConnectionGradients();
    this.drawNeuronErrors();
  };

  this.drawConnectionGradients = function ()
  {
    for (var i in connections) {
      this.drawGradient(connections[i]);
    }
  };

  this.drawGradient = function (data)
  {
    var connection = data.connection;
    var grad = connection.gradient();

    logger.trace("gradient for " + connection.name() + " = " + grad);

    var thickness = 5;
    var magnitude = 1;
    while (Math.abs(grad * magnitude) < 1 && thickness > 1) {
      thickness--;
      magnitude = magnitude * 10;
    }

    logger.trace("thickness = " + thickness);

    push();
    strokeWeight(thickness * 2);
    if (grad > 0) {
      fill(255, 0, 0);
      stroke(255, 0, 0);
    } else {
      fill(0, 0, 255);
      stroke(0, 0, 255);
    }
    line(data.x1, data.y1, data.x2, data.y2);
    pop();
  };

  this.drawNeuronErrors = function (data)
  {
    for (var i in neurons) {
      this.drawNeuronError(neurons[i]);
    }
  };

  this.drawNeuronError = function (data)
  {
    var neuron = data.neuron;
    logger.trace("bp error for " + neuron.name() + " = " + neuron.error());

    grey = Math.abs(neuron.error()) * 255;
    push();
    fill(grey, grey, grey);
    stroke(0, 0, 0);
    ellipse(
      data.x,
      data.y,
      data.size,
      data.size
    );
    pop();
  }
};
