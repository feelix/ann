var connectionNumber = 0;
function Connection (n, w) {


  var neuron = n;
  var weight = w;
  var gradient = 0.0;
  var name = 'C' + connectionNumber++ + 'to' + neuron.name();

  this.out = function () {
    return neuron.activation();
  };

  this.adjust = function(correction) {
    weight -= correction;
  };

  this.setGradient = function(g) {
    gradient = g;
  };

  this.toString = function() {
    return name;
  };

  this.setWeight = function(w) {
    weight = w;
  }

  this.gradient = function() {return gradient};
  this.weight = function() {return weight;};
  this.neuron = function() {return neuron;};
  this.name = function() {return name;};
}
