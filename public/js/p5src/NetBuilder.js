function NetBuilder() {

  this.build3LayerNet = function(inSize, hiddenSize, outSize, learningRate)
  {
    var outBiasNeuron = new InputNeuron(1.0);
    var hiddenBiasNeuron = new InputNeuron(1.0);
    var inLayer = this.buildInputLayer(inSize);
    var hiddenLayer = this.buildHiddenLayer(hiddenBiasNeuron, inLayer, hiddenSize);
    var outLayer = this.buildOutputLayer(outBiasNeuron, hiddenLayer, outSize);

    return new Network([inLayer, hiddenLayer, outLayer], [hiddenBiasNeuron, outBiasNeuron], learningRate);
  };

  this.buildInputLayer = function(inSize)
  {
    logger.log("build input layer, size=", inSize);

    var layer = [];
    for (var i = 1; i <= inSize; i++) {
      layer.push(new InputNeuron(0.0));
    }

    logger.log("INPUT Layer is ", layer);
    return layer;
  };

  this.buildHiddenLayer = function(biasNeuron, inLayer, hiddenSize)
  {
    logger.log("build hidden layer: bias, inLayer, size=", biasNeuron, inLayer, hiddenSize);

    var layer = [];
    var toIn = [];
    var conn;
    for (var h = 1; h <= hiddenSize; h++) {
      toIn = [];
      var inputs = inLayer.length;
      for (var i in inLayer) {
        toIn.push(new Connection(inLayer[i], randomGaussian(0, 1/Math.sqrt(inputs))));
      }
      toIn.push(new Connection(biasNeuron, randomGaussian(0, 1)));
      layer.push(new Neuron(0.0, toIn));
    }

    logger.log("HIDDEN Layer is ", layer);
    return layer;
  };

  this.buildOutputLayer = function(biasNeuron, hiddenLayer, outSize)
  {
    logger.log("build output layer: bias, hiddenLayer, size=", biasNeuron, hiddenLayer, outSize);

    var layer = [];
    var toHiddenX = [];
    for (var o = 1; o <= outSize; o++) {
      toHiddenX = [];
      var inputs = hiddenLayer.length;
      for (var h in hiddenLayer) {
        conn = new Connection(hiddenLayer[h], randomGaussian(0, 1/Math.sqrt(inputs)));
        toHiddenX.push(conn);
      }
      var conn = new Connection(biasNeuron, randomGaussian(0, 1));
      toHiddenX.push(conn);
      layer.push(new Neuron(0.0, toHiddenX));
    }

    logger.log("OUTPUT Layer is ", layer);
    return layer;
  };
}
