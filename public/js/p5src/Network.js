function Network(l, b, r) {

  var layers = l;
  var biases = b;
  var learningRate = r;

  this.layers = function()
  {
    return layers;
  };

  this.biases = function() {
    return biases;
  };

  this.set = function(layer, neuron, value)
  {
    layers[layer][neuron].set(value);
  };

  this.get = function(layer, neuron)
  {
    return layers[layer][neuron].activation();
  };

  this.feedForward = function ()
  {
    for (var l = 1; l < layers.length; l++) {
      layer = layers[l];
      for (var n in layer) {
        neuron = layer[n];
        neuron.feedForward();
      }
    }
  };

  this.backPropogate = function (target)
  {
    var oLayerIdx = layers.length - 1;
    var oLayerSize = layers[oLayerIdx].length;
    if (!(target.length == oLayerSize)) {
      console.log("Mismatched output layer and target sizes")
    }

    // treat o layer separate as have to sum the errors to pass to inner layers
    var error = 0.0;
    var totalError = 0.0;
    for (var i = 0; i < oLayerSize; i++) {
      oNeuron = layers[oLayerIdx][i];
      error = oNeuron.activation() - target[i];
      totalError += error;
      oNeuron.backPropogate(error, learningRate);
    }

    for (var l = oLayerIdx - 1; l >= 1; l--) {
      var hiddenLayer = layers[l];
      for (var n in hiddenLayer) {
        hiddenNeuron = hiddenLayer[n];
        hiddenNeuron.backPropogate(totalError, learningRate);
      }
    }
  };

  this.setInitialGaussianWeights = function ()
  {
    for (var l = layers.length - 1; l > 0; l--) {
      var layer = layers[l];
      this.setInitialGaussianWeightsInLayer(layer);
    }
  };

  this.setInitialGaussianWeightsInLayer = function (layer)
  {
    for (n in layer) {
      this.setInitialGaussianWeightsIntoNeuron(layer[n], layer[n].connections().length - 1);
    }
  };

  this.setInitialGaussianWeightsIntoNeuron = function (neuron, inputs)
  {
    var connections = neuron.connections();
    for (c in connections) {
      var connection = connections[c];
      var connectedNeuron = connection.neuron();
      var weight;
      if (this.isBiasNeuron(connectedNeuron)) {
        weight = randomGaussian(0, 1);
        connection.setWeight(weight);
      } else {
        weight = randomGaussian(0, 1 / (Math.sqrt(inputs)));
        connection.setWeight(weight);
      }
    }
  };

  this.isBiasNeuron = function (neuron)
  {
    for (var b in this.biases) {
      var bias = this.biases[b];
      if (bias === neuron) {
        return true;
      }
    }
    return false;
  };

}
