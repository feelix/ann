WeightRenderer = function(netMap) {

  var neurons = netMap.neurons;
  var connections = netMap.connections;

  this.draw = function ()
  {
    this.drawConnections();
    this.drawNeurons();
  };

  this.drawConnections = function ()
  {
    for (var name in connections) {
      this.drawConnection(connections[name]);
    }
  };

  this.drawConnection = function (data)
  {
    logger.log("drawConnection " + data.connection.name());

    var connection = data.connection;
    var weight = connection.weight();
    push();
    if (weight > 0) {
      fill(100 + 155 * weight / 4, 0, 0);
      stroke(100 + 155 * weight / 4, 0, 0);
    } else {
      fill(0, 0, 100 - 155 * weight / 4);
      stroke(0, 0, 100 - 155 * weight / 4);
    }
    line(data.x1, data.y1, data.x2, data.y2);
    pop();
  };

  this.drawNeurons = function ()
  {
    for (var name in neurons) {
      this.drawNeuron(neurons[name]);
    }
  };

  this.drawNeuron = function (data)
  {
    logger.log("drawNeuron for data " + data.neuron.name());

    var neuron = data.neuron;
    var greyScale = neuron.activation() * 255;
    push();
    fill(greyScale, greyScale, greyScale);
    stroke(0, 0, 0);
    ellipse(
      data.x,
      data.y,
      data.size,
      data.size
    );
    pop();
  };

}
