var neuronNumber = 0;
function Neuron (a, c) {

  var activation = a;
  var connections = c;
  var error = 1;
  var name = 'N' + neuronNumber++;

  this.set = function(a)
  {
    activation = a;
  };

  this.activation = function() {
    return activation;
  };

  this.error = function() {
    return error;
  };

  this.connect = function(c) {
    connections.push(c);
  };

  this.connections = function() {
    return connections;
  };

  this.feedForward = function() {
    var a = this.activate();
    //logger.log(this.name() + " ACTIVATION = " + a);
    activation = a;
  };

  this.backPropogate = function(e, learningRate) {
    //logger.log(this.name() + " bp error = " + e + " at rate " + learningRate);
    error = e;
    for (var i in connections) {
      // record error and gradient so can show them in net viz
      var connection = connections[i];
      var gradient = e
        * this.deltaOutWRTSum()
        * this.deltaSumWRTConnectedNeuron(connection);
      connection.adjust(learningRate * gradient);
      connection.setGradient(gradient);
    }

    logger.log("ADJUST:", gradient);
  };

  this.deltaOutWRTSum = function() {
    return activation * (1 - activation);
  };

  this.deltaSumWRTConnectedNeuron = function(connection) {
    return connection.out();
  };

  this.activate = function() {
    return this.sigmoid(this.sum());
  };

  this.sum = function() {
    sum = 0.0;

     //logger.log("Neuron-CONNECTIONS ", this.connections());

    for (var i in connections) {
      c = connections[i];
      //logger.log(c.name() + "!!!!!! Connection Weight = " + c.weight());
      sum += c.out() * c.weight();
    }

    logger.log(this.name() + " SUM = " + sum);
    return sum;
  };

  this.sigmoid = function(sum) {
    return 1 / (1 + Math.exp(-1 * sum))
  };

  this.toString = function() {
    return name;
  };

  this.name = function() {
    return name;
  };
}
