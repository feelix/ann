// var neuronNumber = 0; -- see Neuron.js
function InputNeuron (a) {

  var activation = a;
  var name = 'I' + neuronNumber++;
  var error = 0;

  this.set = function(a)
  {
    activation = a;
  };

  this.activation = function() {
    return activation;
  };

  this.error = function() {
    return error;
  };

  this.toString = function() {
    return name;
  };

  this.name = function() {
    return name;
  };
}
