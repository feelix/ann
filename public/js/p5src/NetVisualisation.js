/**
 * Given the layers and biases for a simple (non-convolutional) multi layer ANN
 * builds a connection map suitable for visually representing the ANN
 *
 * so do we need to make this just a connection builder and pass them on to
 * other visualisation tools?
 *
 * @param layers
 * @param biases
 * @constructor
 */
function NetVisualisation(network, ls, ns, bs) {

  var layers = network.layers();
  var biases = network.biases();
  var layerSize = ls;
  var neuronSize = ns;
  var biasSize = bs;

  this.build = function(x, y)
  {
    var neurons = this.placeNeurons(x, y);
    return {
      neurons: neurons,
      connections: this.placeConnections(x, y, neurons)
    }
  };

  /**
   * build neurons[] : array of neuron --> {x, y, size} for all neurons in all layers
   * assumes each layer has 1 bias neuron at the end!
   */
  this.placeNeurons = function (x, y)
  {
    var neurons = [];
    var lx = x + layerSize / 2;
    var ly = y + layerSize / 2;
    for (var l in layers) {
      nx = lx;
      ny = ly;
      for (var n in layers[l]) {
        var neuron = layers[l][n];
        neurons[neuron.name()] = {neuron: neuron, x: nx, y: ny, size: neuronSize};
        ny += layerSize;
      }
      // biases, for all but input layer, natch
      if (!(l == 0)) {
        var biasNeuron = biases[l - 1];
        neurons[biasNeuron.name()] = {
          neuron: biasNeuron,
          x: nx - layerSize / 2,
          y: ny + layerSize / 2,
          size: biasSize
        };
      }
      lx += layerSize;
    }

    return neurons;
  };

  /**
   * build connections[] : array of connection --> (x1, y1, x2, y2) from o/p layer back to i/p layer
   */
  this.placeConnections = function (x, y, neurons)
  {
    var connections = [];
    for (var l = layers.length - 1; l >= 1; l--) {
      for (n in layers[l]) {
        neuron = layers[l][n];
        var neuronConnections = neuron.connections();
        for (c in neuronConnections) {
          var connection = neuronConnections[c];
          var otherNeuron = connection.neuron();
          var startX = neurons[neuron.name()].x;
          var startY = neurons[neuron.name()].y;
          var endX = neurons[otherNeuron.name()].x;
          var endY = neurons[otherNeuron.name()].y;
          connections[connection.name()] = {
            connection: connection,
            x1: startX,
            y1: startY,
            x2: endX,
            y2: endY
          };
        }
      }
    }

    return connections;
  };

}
