/**
 * Parameters and their handlers and the dom element handles for ANN Controller
 * ----------------------------------------------------------------------------
 *
 * COULD encapsulate in Controller = function() {} and call them
 * directly from setup() and draw() - ,maybe?
 */

// ----- handles [p5 rule: MUST be set in setup() for draw() to access them]
var $inputSize;
var $hiddenSize;
var $outputSize;
var $reBuild;

var $operator;
var $runs;
var $accuracy;
var $totalRuns;
var $trainOrPause;
var $reset;

// UI Display, output only
var $fails;
var $passes;
var $dunnos;
var $lastError;
var $passRate;

// ----- parameters
// -- err - dont do this, get initial values from the html!
var operator = 'NAND';
var isTraining = false;
var runs = 1000;
var accuracy = 10;
var totalRuns = 0;

var fails = 0;
var passes = 0;
var dunnos = 0;
var lastError = 0;
var passRate = 0;

// ----- handlers
toggleTrainOrPause = function () {
  (isTraining) ? stopTraining() : startTraining();
};

stopTraining = function () {
  isTraining = false;
  noLoop();
};

startTraining = function () {
  console.log("start training");
  totalRuns = 0;
  passes = 0;
  fails = 0;
  dunnos = 0;
  runs = $runs.value();
  operator = $operator.value();
  accuracy = $accuracy.value();
  if (! isTraining) loop();
  isTraining = true;
};

updateUI = function() {
  //console.log("UpdateUI-IN");
  $fails.value(fails);
  $passes.value(passes);
  $dunnos.value(dunnos);
  $lastError.value(lastError);
  $totalRuns.value(totalRuns);
  $passRate.value(passRate);
};

changeOperator = function() {
  operator = $operator.value();
};

reset = function () {
  stopTraining();
  clearWeights();
  redraw();
};

clearWeights = function() {
  network.setInitialGaussianWeights();
};

var learningRate = 1;
var inputSize = 2;
var hiddenSize = 2;
var outputSize = 1;

var builder;
var network;
var visualisation;
var renderer;

function reBuild() {
  stopTraining();
  buildNetworkFromUISettings();
  redraw();
}

function buildNetworkFromUISettings() {

  inputSize = $inputSize.value();
  hiddenSize = $hiddenSize.value();
  outputSize = $outputSize.value();

  builder = new NetBuilder();
  network = builder.build3LayerNet(inputSize, hiddenSize, outputSize, learningRate);

  visualisation = new NetVisualisation(network, 100, 50, 30);

  weightRenderer = new WeightRenderer(visualisation.build(0, 0));
  gradientRenderer = new GradientRenderer(visualisation.build(500, 0));
}

/**
 * HAVE to add the elements to canvas in setup()
 */
function setup() {

  c = createCanvas(1000, 800);
  c.parent('#visualisation');

  // setup and build the UI
  $operator = select('#operator');
  $operator.value(operator);
  $operator.changed(changeOperator);

  $runs = select('#runs');
  $runs.value(runs);
  $totalRuns = select('#total-runs');
  $totalRuns.value(totalRuns);
  $accuracy = select('#accuracy');
  $accuracy.value(accuracy);

  $trainOrPause = select('#train-or-pause');
  $trainOrPause.mouseClicked(toggleTrainOrPause);

  $fails = select('#fails');
  $fails.value(fails);
  $passes = select('#passes');
  $passes.value(passes);
  $dunnos = select('#dunnos');
  $dunnos.value(dunnos);
  $passRate = select('#pass-rate');
  $passRate.value(passRate);

  $lastError = select('#last-error');
  $lastError.value(lastError);

  $reset = select('#reset');
  $reset.mouseClicked(reset);

  noFill();
  stroke(0, 0, 0);
  rect(0, 0, 799, 599);

  noLoop();

  // setup and build the network
  $reBuild = select('#reBuild');
  $inputSize = select('#inputSize');
  $inputSize.value(inputSize);
  $hiddenSize = select('#hiddenSize');
  $outputSize = select('#outputSize');
  $reBuild.mouseClicked(reBuild);

  buildNetworkFromUISettings();
}

/**
* ===== Processing Draw
*
* using draw() to do training and test runs, updating after each
* run where each run is a single frame
*/
function draw() {

  if (isTraining) train();

  clear();

  weightRenderer.draw();
  gradientRenderer.draw();

  updateUI();
}

function Activation(value) {

  var activation = value;
  var on = (activation > 0.5);

  this.fromBool = function(state) {
    on = state;
    activation = state ? 0.99 : 0.01
  };

  this.toBool = function() {
    return on;
  };

  this.toBoolOrUndefined = function(accuracy) {
    if (Math.abs(activation - 0.99) < accuracy) return true;
    if (Math.abs(activation - 0.01) < accuracy) return false;
    return "not sure";
  };

  this.toFloat = function() {
    return on ? 0.99 : 0.01;
  };

  this.activation = function() {
    return activation;
  }
}

function applyOperator(op, a, b) {
  if (op === "NAND") return ! (a && b) ;
  if (op === "AND") return (a && b);
  if (op === "XOR") return (!a && b) || (a && !b);
  if (op === "OR") return (a || b);
  //if (op == "HALF_ADDER") return ();
  //if (op == "FULL-ADDER") return ();
  //if (op == "") return ();
}
function train() {

  var a = new Activation(Math.random());
  var b = new Activation(Math.random());

  var c = new Activation(0);
  c.fromBool(applyOperator(operator, a.toBool(), b.toBool()));

  var target = c.toFloat();

  network.set(0, 0, a.toFloat());
  network.set(0, 1, b.toFloat());
  network.feedForward();

  var result = new Activation(network.get(2, 0));

  var error = Math.abs(result.activation() - target);
  lastError = (error * error)/2 ;

  var t = new Activation(target);
  if (result.toBoolOrUndefined(accuracy/100) === "not sure") {
    dunnos++;
  } else {
    (t.toBool() === result.toBool()) ? passes++ : fails++;
  }
  network.backPropogate([target]);

  totalRuns++;
  passRate = (passes/totalRuns) * 100;

  if (totalRuns >= runs) stopTraining();
}
