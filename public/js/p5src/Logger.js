var logger = new RealLogger();

function RealLogger()
{
  var logging = true;

  this.on = function() {
    logging = true;
  };

  this.off = function()
  {
    logging = false;
  }

  this.log = function(msg)
  {
    if (logging) console.log(msg);
  }

  this.trace = function(msg)
  {
    console.log(msg);
  }
}
