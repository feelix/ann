<?php

namespace ANN;

use ANN\Interfaces\InputNeuronInterface;
use ANN\Traits\InputNeuronTrait;

class InputNeuron implements InputNeuronInterface
{
    use InputNeuronTrait;

    /** @var float */
    private $activation;

    /** @param float $activation */
    public function __construct(float $activation)
    {
        $this->set($activation);
    }
}
