<?php

namespace ANN\Traits;

trait InputNeuronTrait
{
    /** @param float $activation */
    public function set(float $activation)
    {
//        echo "set input neuron to $activation\n";
        $this->activation = $activation;
    }

    /** @return float */
    public function activation() : float
    {
//        echo "read input neuron as {$this->activation}\n";
        return $this->activation;
    }
}
