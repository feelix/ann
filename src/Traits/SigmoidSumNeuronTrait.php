<?php

namespace ANN\Traits;

use ANN\Connection;

/**
 * @todo make these protected, after tdd dev phase
 */
trait SigmoidSumNeuronTrait
{
    /** @return float */
    public function sum() : float
    {
        $sum = 0.0;
        /** @var Connection $connection */
        foreach ($this->connections as $connection) {
            $sum = $sum + $connection->out() * $connection->weight();
        }

//        echo "SUM=$sum";
        return $sum;
    }

    /**
     * @param float $sum
     *
     * @return float
     */
    public function sigmoid(float $sum) : float
    {
        return (float) 1 / (1 + exp(- $sum));
    }

    /** @return float */
    public function activate() : float
    {
        $sigmoidSum = $this->sigmoid($this->sum());

//        echo "\t -->sigmoid--> $sigmoidSum\n";
        return $sigmoidSum;
    }

    /**
     * @param float $target
     *
     * @return float
     */
    public function deltaErrorWRTOut(float $target) : float
    {
        return $this->activation() - $target;
    }

    /** @return float */
    public function deltaOutWRTSum() : float
    {
        return $this->activation() * (1 - $this->activation());
    }

    /**
     * @param Connection $c
     *
     * @return float
     */
    public function deltaSumWRTConnection(Connection $c) : float
    {
        return $c->out();
    }

    /**
     * @param float $target
     * @param Connection $c
     */
    public function errorGradientWRTConnection(float $target, Connection $c) : float
    {
        return $this->deltaErrorWRTOut($target) *
               $this->deltaOutWRTSum() *
               $this->deltaSumWRTConnection($c);
    }
}
