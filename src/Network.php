<?php

namespace ANN;

use ANN\Interfaces\NeuronInterface;

class Network
{
    /** @var NeuronInterface[][] */
    private $layers;
    private $learningRate;

    /**
     * @param array NeuronInterface[][] $layerSizes
     */
    public function __construct(array $layers)
    {
        $this->layers = $layers;
    }

    public function feedForward()
    {
        for ($i = 1; $i < count($this->layers); $i++) {
            /** @var NeuronInterface[][] $layers */
            foreach ($this->layers[$i] as $neuron) {
                $neuron->feedForward();
            }
        }
    }

    /**
     * @param float[] $target
     */
    public function backPropagate(array $target)
    {
        $outputIndex = count($this->layers) - 1;
        $outputSize = count($this->layers[$outputIndex]);
        // @todo replace rubbish exception
        if (count($target) !== $outputSize) throw new \Exception("Network::backPropagate incompatible target dimension");

        $totalError = 0.0;
        for ($i = 0; $i < $outputSize; $i++) {

            /** @var NeuronInterface $outputNeuron */
            $outputNeuron = $this->layers[$outputIndex][$i];
            $error = $outputNeuron->activation() - $target[$i];
            $totalError = $totalError + $error;

            $connections = $outputNeuron->connections();
//            echo "===============================================================================================\n";
//            echo "error $error, activation={$outputNeuron->activation()}, target={$target[$i]}\n";
//            echo "   bp error: $error and LR: $learningRate\n";
//            echo "   weight 0 {$connections[0]->weight()} weight 1 = {$connections[1]->weight()} weight 2 = {$connections[2]->weight()} weight 3 = {$connections[3]->weight()} weight 4 = {$connections[4]->weight()} \n";
//            echo "------------------------------------------------------------------------------------------------\n";

            $outputNeuron->backPropagate($error, $this->learningRate);
        }

        // now we bp sum of errors through all hidden layers
        for ($i = $outputIndex - 1; $i > 0; $i--) {
            for ($j = 0; $j < count($this->layers[$i]); $j++ ){

                /** @var Neuron $neuron */
                $hiddenNeuron = $this->layers[$i][$j];
//                echo "at hidden neuron layer $i, neuron $j activation={$hiddenNeuron->activation()}\n";
//                echo "Total Error: $totalError\n";
                $connections = $hiddenNeuron->connections();
//                echo "   weight 0 {$connections[0]->weight()} * {$connections[0]->out()} weight 1 = {$connections[1]->weight()} * {$connections[1]->out()}\n";

                $hiddenNeuron->backPropagate($totalError, $this->learningRate);
            }
        }
//        echo "===============================================================================================\n";
    }
    
    public function learningRate(float $rate)
    {
        $this->learningRate = $rate;
    }
    
}
