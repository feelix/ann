<?php

namespace ANN;

use ANN\Interfaces\InputNeuronInterface;

class Connection
{
    /** @var Neuron */
    private $neuron;

    /** @var float */
    private $weight;

    /**
     * @param InputNeuronInterface $neuron
     * @param float $weight
     */
    public function __construct(InputNeuronInterface $neuron, float $weight)
    {
        $this->neuron = $neuron;
        $this->weight = $weight;
    }

    /** @return float */
    public function out() : float
    {
        return $this->neuron->activation();
    }

    /** @return float|float */
    public function weight() : float
    {
        return $this->weight;
    }

    /** @param float $change */
    public function adjust(float $change)
    {
//        echo "W={$this->weight},  deltaW = $change\n";
        $this->weight = $this->weight - $change;
    }
}
