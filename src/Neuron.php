<?php

namespace ANN;

use ANN\Interfaces\NeuronInterface;
use ANN\Traits\InputNeuronTrait;
use ANN\Traits\SigmoidSumNeuronTrait;

class Neuron implements NeuronInterface
{
    use InputNeuronTrait;
    use SigmoidSumNeuronTrait;

    /** @var float */
    private $activation = 0.0;

    /** @var Connection[] */
    private $connections;

    /** @param Connection[] $connections */
    function __construct(array $connections)
    {
        $this->connect($connections);
    }

    /** @param array $connections */
    public function connect(array $connections)
    {
        $this->connections = $connections;
    }

    /** @return Connection[] */
    public function connections()
    {
        return $this->connections;
    }

    public function feedForward()
    {
        $this->set($this->activate());
    }

    /**
     * @param float $error
     * @param float $learningRate
     */
    public function backPropagate(float $error, float $learningRate)
    {
        /** @var Connection $connection */

        foreach ($this->connections as $connection) {
            $weight = $connection->weight();
            $out = $connection->out();
            $dSum = $this->deltaOutWRTSum();
            $dConn = $this->deltaSumWRTConnection($connection);


            $grad = $error *
                    $this->deltaOutWRTSum() *
                    $this->deltaSumWRTConnection($connection);
//            echo ">>>grad $grad = $error x $dSum x $dConn [w=$weight o=$out]\n";
//            echo "-error = $error\n";
//            echo "-dSum = $dSum\n";
//            echo "-dConn = $dConn\n";
            $connection->adjust($learningRate * $grad);
        }
//        echo "----\n";
    }

}
