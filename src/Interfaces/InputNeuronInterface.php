<?php

namespace ANN\Interfaces;

interface InputNeuronInterface
{
    public function set(float $activation);

    public function activation() : float;
}
