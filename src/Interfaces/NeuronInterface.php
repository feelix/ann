<?php

namespace ANN\Interfaces;

use ANN\Connection;

interface NeuronInterface extends InputNeuronInterface
{
    /** @param array $connections */
    public function connect(array $connections);

    /** @return Connection[] */
    public function connections(); //@todo should not need to reveal this to network!!!

    /** @return mixed */
    public function sum() : float;

    /** @return mixed */
    public function activate() : float;

    public function feedForward();

    /**
     * @param float $error
     * @param float $learningRate
     */
    public function backPropagate(float $error, float $learningRate);

    /**
     * @param float $target
     *
     * @return float
     */
    public function deltaErrorWRTOut(float $target) : float;

    /** @return float */
    public function deltaOutWRTSum() : float;

    /**
     * @param Connection $c
     *
     * @return float
     */
    public function deltaSumWRTConnection(Connection $c) : float;

    /**
     * @param float $target
     * @param Connection $c
     *
     * @return float
     */
    public function errorGradientWRTConnection(float $target, Connection $c) : float;
}
