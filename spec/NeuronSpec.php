<?php

namespace spec\ANN;

use ANN\Connection;
use ANN\Interfaces\InputNeuronInterface;
use ANN\Interfaces\NeuronInterface;
use ANN\Neuron;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Console\Input\InputInterface;

/**
 * @mixin Neuron
 */
class NeuronSpec extends ObjectBehavior
{
    private $sum;

    private $sigmoid;

    const OUT1 = 1.0;

    const WEIGHT_OF_1 = 0.5;

    const OUT2 = 2.0;

    const OUT3 = 3.0;

    const ACTIVATION = 3.0;

    function it_constructs_with_empty_connections()
    {
        $this->beConstructedWith([]);
        $this->shouldHaveType(InputNeuronInterface::class);
        $this->shouldHaveType(NeuronInterface::class);
    }

    function it_has_an_empty_sum_with_no_connections()
    {
        $this->beConstructedWith([]);
        $this->sum()->shouldBe(0.00);
    }

    function it_has_a_sigmoid_of_half_with_no_connections()
    {
        $this->beConstructedWith([]);
        $this->sigmoid(0.0)->shouldBe(self::WEIGHT_OF_1);
    }

    function it_has_a_half_output_with_no_connections()
    {
        $this->beConstructedWith([]);
        $this->activate()->shouldBe(self::WEIGHT_OF_1);
    }

    function it_sums_a_connected_neurons_weighted_output(Connection $connection)
    {
        $connection->out()->willReturn(self::OUT1);
        $connection->weight()->willReturn(self::WEIGHT_OF_1);

        $this->beConstructedWith([$connection]);

        $this->sum()->shouldReturn(self::WEIGHT_OF_1);
    }

    function let(Connection $c1, Connection $c2, Connection $c3)
    {
        $c1->out()->willReturn(self::OUT1);
        $c1->weight()->willReturn(self::WEIGHT_OF_1);

        $c2->out()->willReturn(self::OUT2);
        $c2->weight()->willReturn(self::WEIGHT_OF_1);

        $c3->out()->willReturn(self::OUT3);
        $c3->weight()->willReturn(self::WEIGHT_OF_1);

        $this->sum = 3;
        $this->sigmoid = (float) 1/(1 + exp(-3));

        $this->beConstructedWith([$c1, $c2, $c3]);
    }

    function it_sums_connected_neurons_output(Connection $c1, Connection $c2, Connection $c3)
    {
        $this->sum()->shouldReturn(self::ACTIVATION);
    }

    function it_calculates_sigmoid_of_sum(Connection $c1, Connection $c2, Connection $c3)
    {
        $this->sigmoid($this->sum)->shouldReturn($this->sigmoid);
    }

    function it_feeds_forward_and_remembers_its_new_activation(Connection $c1, Connection $c2, Connection $c3)
    {
        $c1->out()->shouldBeCalled();
        $c2->out()->shouldBeCalled();
        $c3->out()->shouldBeCalled();
        $c1->weight()->shouldBeCalled();
        $c2->weight()->shouldBeCalled();
        $c3->weight()->shouldBeCalled();

        $this->feedforward();

        $this->activation()->shouldReturn($this->sigmoid);
    }

    function it_calculates_delta_error_wrt_out_for_a_given_target()
    {
        $this->feedforward();

        $target = 1;
        $out = $this->sigmoid;

        $this->deltaErrorWRTOut($target)->shouldBe($out - $target);
    }

    function it_calculates_delta_out_wrt_expected_activation()
    {
        $this->feedforward();

        $activation = $this->sigmoid;

        $this->deltaOutWRTSum()->shouldBe($activation * (1 - $activation));
    }

    function it_calculates_delta_sum_wrt_each_weight(Connection $c1, Connection $c2, Connection $c3)
    {
        $c1->out()->shouldBeCalled();
        $c2->out()->shouldBeCalled();
        $c3->out()->shouldBeCalled();

        $this->deltaSumWRTConnection($c1)->shouldReturn(self::OUT1);
        $this->deltaSumWRTConnection($c2)->shouldReturn(self::OUT2);
        $this->deltaSumWRTConnection($c3)->shouldReturn(self::OUT3);
    }

    function it_calculates_the_error_gradient_wrt_each_connection(Connection $c1, Connection $c2, Connection $c3)
    {
        $this->feedforward();

        $target = 1.0;
        $this->errorGradientWRTConnection($target, $c1)->shouldReturn($this->gradientFunction($target, $this->sigmoid, self::OUT1));
        $this->errorGradientWRTConnection($target, $c2)->shouldReturn($this->gradientFunction($target, $this->sigmoid, self::OUT2));
        $this->errorGradientWRTConnection($target, $c3)->shouldReturn($this->gradientFunction($target, $this->sigmoid, self::OUT3));
    }

    function it_back_propagates_a_given_error(Connection $c1, Connection $c2, Connection $c3)
    {
        $this->feedforward();

        $target = 1.0;
        $error = $this->sigmoid - $target;
        $learningRate = 1;

        $c1->adjust($this->calculateWeightAdjustment($target, $this->sigmoid, self::OUT1, $learningRate))->shouldBeCalled();
        $c2->adjust($this->calculateWeightAdjustment($target, $this->sigmoid, self::OUT2, $learningRate))->shouldBeCalled();
        $c3->adjust($this->calculateWeightAdjustment($target, $this->sigmoid, self::OUT3, $learningRate))->shouldBeCalled();

        $this->backPropagate($error, $learningRate);
    }

    private function calculateWeightAdjustment(float $target, float $output, float $input, $learningRate)
    {
        $grad = $this->gradientFunction($target, $output, $input);

        return $grad * $learningRate;
    }

    /**
     * @param float $target
     * @param float $activation
     * @param Connection $c
     *
     * @return string
     */
    private function gradientFunction(float $target, float $output, float $input) : float
    {
        return ($output - $target) *
               ($output * (1 - $output)) *
               $input;
    }
}
