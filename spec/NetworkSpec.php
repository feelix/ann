<?php

namespace spec\ANN;

use ANN\Connection;
use ANN\InputNeuron;
use ANN\Network;
use ANN\Neuron;
use PhpSpec\ObjectBehavior;
use PHPUnit_Framework_Assert;
use Prophecy\Argument;

/**
 * @mixin Network
 */
class NetworkSpec extends ObjectBehavior
{
    const INITIAL_WEIGHT = 1.0;
    const INITIAL_ACTIVATION = 1.0;

    /** @var InputNeuron  */
    private $in;

    /** @var Neuron  */
    private $hidden;

    /** @var Neuron */
    private $out;

    /** @var Connection */
    private $toIn;

    /** @var Connection */
    private $toHidden;

    /** @var float */
    private $sigSigmoid;

    /** @var float */
    private $sigmoid;

    function it_is_initializable()
    {
        $this->shouldHaveType(Network::class);
    }

    function let()
    {
        $this->in = new InputNeuron(self::INITIAL_ACTIVATION);
        $this->toIn = new Connection($this->in, self::INITIAL_WEIGHT);
        $this->hidden = new Neuron([$this->toIn]);

        $this->toHidden = new Connection($this->hidden, self::INITIAL_WEIGHT);
        $this->out = new Neuron([$this->toHidden]);

        $this->beConstructedWith([[$this->in], [$this->hidden], [$this->out]]);
    }

    function it_feeds_forward()
    {
        $this->feedForward();

        PHPUnit_Framework_Assert::assertEquals(self::INITIAL_ACTIVATION, $this->in->activation(), "In activation did not stay same", 0.001);

        PHPUnit_Framework_Assert::assertEquals($this->sigmoid(self::INITIAL_ACTIVATION), $this->hidden->activation(), "Hidden activation not sigmoid of 1.0", 0.001);

        PHPUnit_Framework_Assert::assertEquals($this->sigmoid($this->sigmoid(self::INITIAL_ACTIVATION)), $this->out->activation(), "Out activation not sigmoid of sigmoid of 1.0", 0.001);
    }

    private function sigmoid(float $sum)
    {
        return 1.0/(1.0 + exp(- $sum));
    }

    function xit_throws_a_backprop_if_target_does_not_match_output_layer_size()
    {
        $this->shouldThrow(\Exception::class)->during('backPropagate', [1,2,3], 1);
    }

    function xit_asks_for_activation_to_calculate_error()
    {
        // need mocking to do this stuff... hmmmm, shame i invested so much effort in Math Class!

        // ok - worth doing this for multi-neuron, multi-layer networks, soon!
    }

    function it_back_propagates()
    {
        $this->feedForward();

        $target = 0.0;
        $this->learningRate(1.0);
        $this->backPropagate([$target]);

        /*
         * Math Class
         *
         * For a target [] of [1.0]
         * at neuron $out
         *   activation = $this->sigSigmoid
         *   deltaOutWRTSum = activation * (1 - activation)
         *   deltaSumWRTWeight = $hidden->activation() <------ [= $this->sigmoid]
         *   error = activation - target[0]
         *   gradientOfErrorWRTWeight = error * (sigSigmoid * (1 - sigSigmoid)) * sigmoid
         *   SO
         *   weight adjustment is learningRate * gradientOfErrorWRTWeight * gradientOfErrorWRTWeight
         *
         *
         */
        $sig = $this->sigmoid(self::INITIAL_ACTIVATION);
//        echo "sig: $sig\n";

        $sigSig = $this->sigmoid($sig);
//        echo "sigSig: $sigSig\n";

        $error = $sigSig - $target;
//        echo "error: $error\n";

        $deltaWeightToOut = $error * ($sigSig * (1 - $sigSig)) * $sig;
//        echo "deltaWeightToOut: $deltaWeightToOut\n";

        $currentWeight = $this->toHidden->weight();
//        echo "Current weight from out to hidden: $currentWeight\n";

        $newWeightToOut = self::INITIAL_WEIGHT - (1.0 * $deltaWeightToOut);
        PHPUnit_Framework_Assert::assertEquals($newWeightToOut, $this->toHidden->weight(), "New weight from hidden to out has wrong value", 0.001);
    }

}
