<?php

namespace spec\ANN;

use ANN\InputNeuron;
use ANN\Interfaces\InputNeuronInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin InputNeuron
 */
class InputNeuronSpec extends ObjectBehavior
{
    const ACTIVATION = 1.23;

    function let()
    {
        $this->beConstructedWith(self::ACTIVATION);
        $this->shouldHaveType(InputNeuron::class);
        $this->shouldHaveType(InputNeuronInterface::class);
    }

    function it_has_its_initial_activation()
    {
        $this->activation()->shouldBe(self::ACTIVATION);
    }

    function it_can_activated()
    {
        $this->set(3.142);

        $this->activation()->shouldBe(3.142);
    }

}
