<?php

namespace spec\ANN;

use ANN\Connection;
use ANN\Interfaces\InputNeuronInterface;
use ANN\Neuron;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin Connection
 */
class ConnectionSpec extends ObjectBehavior
{
    function let(InputNeuronInterface $neuron)
    {
        $this->beConstructedWith($neuron, 1.0);
    }

    function it_knows_its_neurons_activation_as_out(Neuron $neuron)
    {
        $neuron->activation()->willReturn(1.23)->shouldBeCalled();

        $this->out()->shouldBe(1.23);
    }

    function it_knows_its_neurons_out_from_its_activation_alone(Neuron $neuron)
    {
        $neuron->activate()->shouldNotBeCalled();
        $neuron->activation()->willReturn(1.23)->shouldBeCalled();

        $this->out()->shouldBe(1.23);
    }

    function it_knows_its_weight()
    {
        $this->weight()->shouldBe(1.0);
    }

    function it_adjusts_weight()
    {
        $this->adjust(0.3);

        $this->weight()->shouldBe(0.7);
    }
}
