#Exploring ANN's in PHP and p5

Here's an ongoing exploration of Artificial Neural Networks, initially based on ideas from Daniel Shiffman's The Nature of Code (http://natureofcode.com) but I am also indebted to Michael Nielson's Neural Networks and Deep Learning (http://neuralnetworksanddeeplearning.com)
  
##PHP Implementation

As an old fart recently converted to the joys and benefits of TDD/BDD my first attempts at building ANN's were done using Behat and PHPSpec so that I could have 100% confidence in the networks I created and their efficacy.

So if you check the Behat features you'll notice that I got as far as verifying the efficacy of 3 layer ANNs with 2 inputs and a single output solving the NAND boolean operation. For good measure I had feature steps allowing me to vary the number of hidden layer neurons, and I also hacked together some features (not included - exercise for the reader!) for learning OR, NOT and the AND boolean operations - all succesful so I was confident that my BP and FF algorithms were correct.

However, you'll notice I used a completely (shit) random set of values for the initial weights, thus I was unable to consistently obtain a network that could solve for XOR!

After some research I got to understanding what I would need to do to achieve a minimal ANN that would be 
able to solve XOR - a proper set normally distributed gaussian values with a mean of 0 and a standard deviation of 1/(sqr(#inputs)) where #inputs is the number of inputs to a neuron excluding the bias. 
But, by now I was aching to see my ANN's at work - hell man, what's the point building a brain if you can't see it working, so I turned to a VDD approach! 
 
*VDD = Visualisation Driven Development*

###The Features

Usual story - composer.json has the dependencies you'll need to run the features and spec tests, so you can be confident that the PHP code really works and implements a simple 3 layer ANN with limited capabilities - ie not able to solve XOR unless a more appropriate way of setting the initial weights is developed for it.

##Javascript + p5 (Processing) Development

Shiffman's Nature of Code introduced me to Processing.org - a simple(!) language actually created for artists rather than programmers with the aim of making it as easy as possible to draw amazing stuff on the screen. 

Rather cool it was, but also a bit fiddly and since my JavaScript skills needed honing (understatement for sure) 
when I found there's a JavaScript implementation of Processing I decided to use it to build my own VDD ANN plaything.

So I converted the verified PHP classes for neuron, connection and network into p5 and added some UI and visualisation glue to gradually build up an environment that I think is currently a reasonable stab at a ANN plaything suitable for some 'hello world' style basic ANN experiments!

###The Plaything

Point your browser at ANN/public/index.html to see the plaything.
 
You will initially see the default network with 2 input, 3 hidden and 1 output neurons, set up ready to learn the NAND operation with training runs of 1000 random inputs, click TRAIN/PAUSE to start training. During the run the display will keep updating to show the right, wrong and don't know answers the network provides as well as a colourful display of the feed forward and back propagation algorithms actions at every step.

After 1000 random training sets it will stop. 

Click again for the next run of 1000 examples - this time you should notice that the ANN has 100% accuracy!

This *does* implement an appropriate way of setting the initial weights, using p5's randomGaussian() function to set the initial weights. However, currently I don't so anything to verify that the actual values returned and blindly used for the weights do themselves actually fulfill the mean=0, std dev=1/(sqr(#inputs) for every neuron. This has no effect for the simpler boolean operations you can train the ANN with but if you want it to do the XOR operation you'll have to keep hitting 'reset' until you see that weights from each input neuron are a mix of +ve and -ve (ie red and blue).

I am currently working on improving the 'fitness' of the initial weights.
 




