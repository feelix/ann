<?php

use ANN\Connection;
use ANN\InputNeuron;
use ANN\Network;
use ANN\Neuron;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class NetworkContext implements Context
{
    const APPX_ZERO = 0.01;
    const APPX_ONE  = 0.9;

    /** @var Neuron[] */
    private $input;

    /** @var Neuron[] */
    private $hidden;

    /** @var Neuron[] */
    private $output;

    /** @var Network */
    private $network;
    
    /** @var float */
    private $target;
    
    /** @var float */
    private $learningRate;
    
    /** @var Connection[] */
    private $connections;

    /** @var Neuron */
    private $neuron;

    /** @var Neuron */
    private $out;

    /** @var Neuron */
    private $in;

    /** @var float */
    private $rate;

    /** @var float */
    private $times;

    /** @var Connection */
    private $conn;

    /** @var Connection[] */
    private $toIn;

    /** @var Connection[] */
    private $toHidden;

    /**
     * @Given I am an output neuron
     */
    public function iAmAnOutputNeuron()
    {
    }

    /**
     *
     * === steps for neuron features implemented using a network
     *
     */

    /**
     * @Given I am connected to input neurons with activations and weights
     */
    public function iAmConnectedToInputNeuronsWithActivationsAndWeights(TableNode $table)
    {
        $toInput = [];
        foreach ($table->getHash() as $row) {
            $input         = new InputNeuron($row['activation']);
            $toInput[]     = new Connection($input, $row['weight']);
            $this->input[] = $input;
        }
        $this->output[] = new Neuron($toInput);

        $this->connections = $toInput;
        $this->network = new Network([$this->input, $this->output]);
    }

    /**
     * @Given I am connected to hidden neurons with activations and weights
     */
    public function iAmConnectedToHiddenNeuronsWithActivationsAndWeights(TableNode $table)
    {
        $toHidden = [];
        foreach ($table->getHash() as $row) {
            $hidden = new Neuron([]);
            $toHidden[] = new Connection($hidden, $row['weight']);
            $hidden->set($row['activation']);
            $this->hidden[] = $hidden;
        }
        $this->output[] = new Neuron($toHidden);

        $this->connections = $toHidden;
        $this->network = new Network([$this->hidden, $this->output]);
    }

    /**
     * @When I am connected to an input neuron with activation :arg1 and weight :arg2
     */
    public function iAmConnectedToAnInputNeuronWithActivationAndWeight($activation, $weight)
    {
        $in = new InputNeuron($activation);
        $this->conn = new Connection($in, $weight);
        $this->out = new Neuron([$this->conn]);

        $this->network = new Network([[$in], [$this->out]]);
    }

    /**
     * @When I train for :arg1 times with a learning rate of :arg2
     */
    public function iTrainForTimesWithALearningRateOf($times, $rate)
    {
        $this->times = $times;
        $this->rate = $rate;
        $this->network->learningRate($rate);
        for ($i = 1; $i <= $times; $i ++) {

            $this->network->feedForward();

            $error = $this->out->activation() - $this->target;

            $this->network->backPropagate([$this->target]);
        }
    }

    /**
     * @When I feed forward
     */
    public function iFeedForward()
    {
        $this->network->feedForward();
    }

    /**
     * @Then my sum should be :arg1
     */
    public function mySumShouldBe(float $sum)
    {
        PHPUnit_Framework_Assert::assertSame($sum, $this->output[0]->sum(), "output neuron has wrong sum");
    }

    /**
     * @param $n
     *
     * @return float
     */
    private function sigmoid(float $n)
    {
        return 1 / (1 + exp(- $n));
    }

    /**
     * @Then my out should be the sigmoid of the sum
     */
    public function myOutShouldBeTheSigmoidOfTheSum()
    {
        $sigmoid = $this->sigmoid($this->output[0]->sum());

        PHPUnit_Framework_Assert::assertSame($sigmoid, $this->output[0]->activation(),
            "output neuron has wrong activation");
    }

    /**
     * @When my target value for out is :arg1
     */
    public function myTargetValueForOutIs($target)
    {
        $this->target = $target;
    }

    /**
     * @When my learningRate is :arg1
     */
    public function myLearningrateIs($rate)
    {
        $this->network->learningRate($rate);
    }


    /**
     * @When I backpropagate
     */
    public function iBackpropagate()
    {
        $this->network->backPropagate([$this->target]);
    }

    /**
     * @Then the new weights should be
     */
    public function theNewWeightsShouldBe(TableNode $table)
    {
        foreach ($table->getHash() as $row) {
            $activation          = $row['activation'];
            $oldWeight           = $row['oldweight'];
            $newWeight           = $row['newweight'];
            $foundOriginalNeuron = false;
            foreach ($this->connections as $c) {
                if ($c->out() == $activation) {
                    // calculate the adjustment from first principal (put into table later)
                    $adjustment     = Calculator::calculateWeightAdjustment(
                        $this->target,
                        $this->output[0]->activation(),
                        $c->out(),
                        1
                    );
                    $expectedWeight = $oldWeight - $adjustment;
//                    echo "adjustment for activation $activation is $adjustment\n";
//                    echo "new weight = $expectedWeight\n";
                    PHPUnit_Framework_Assert::assertEquals($expectedWeight, $c->weight(),
                        "new weight not what was calculated");
                    PHPUnit_Framework_Assert::assertEquals($newWeight, $c->weight(),
                        "new weight not what was expected");
                    $foundOriginalNeuron = true;
                }
            }
            if (! $foundOriginalNeuron) {
                PHPUnit_Framework_Assert::assertTrue(false,
                    "Could not find the connection with an activation of $activation");
            }
        }
    }

    /**
     * @Then echo my status
     */
    public function echoMyStatus()
    {
        $msg   = "Training Runs: {$this->times} with Learning Rate: {$this->rate}\n";
        $error = ($this->out->activation() - $this->target) * 100;
        echo "$msg\n";
        echo ">> Output = {$this->out->activation()}\n";
        echo ">> Weight = {$this->conn->weight()}\n";
        echo ">> Error%  = $error\n\n";
    }




    /**
     *
     * ===== for network feature
     *
     */





    /**
     * now wit bias ...
     * @Given I am a :arg1 layer ANN with :arg2 Input, :arg3 Hidden and :arg4 Output neurons
     */
    public function iAmALayerAnnWithInputHiddenAndOutputNeurons(int $layers, int $inputSize, int $hiddenSize, int $outputSize)
    {
        if ($layers !== 3) {
            throw new PendingException("Barp!!!");
        }

        /** @var Connection[] $toIn */
        $toIn = [];
        for ($i = 1; $i <= $inputSize; $i ++) {
            $in            = new InputNeuron(0.0);
            $this->input[] = $in;
        }

        $hiddenBias = new InputNeuron(1.0);
        for ($h = 1; $h <= $hiddenSize; $h++) {
            /** @var Connection[] $toIn */
            $toIn = [];
            for ($i = 0; $i < $inputSize; $i++) {
                $toIn[] = new Connection($this->input[$i], $this->randomWeight());
//                $toIn[] = new Connection($this->input[$i], 1.0);
            }
            $toIn[] = new Connection($hiddenBias, $this->randomWeight());
            $neuron         = new Neuron($toIn);
            $this->hidden[] = $neuron;
        }

        $toHidden = [];
        $inputBias = new InputNeuron(1.0);
        for ($o = 1; $o <= $outputSize; $o++) {
            for ($h = 0; $h < $hiddenSize; $h++) {
                $toHidden[] = new Connection($this->hidden[$h], $this->randomWeight());
//                $toHidden[] = new Connection($this->hidden[$h], 1.0);
            }
            $toHidden[] = new Connection($inputBias, $this->randomWeight());
            $this->output[] = new Neuron($toHidden);
        }

        $this->toIn = $toIn;
        $this->toHidden = $toHidden;
        $this->network = new Network([$this->input, $this->hidden, $this->output]);
    }

    /**
     * @When I train for :arg1 times with a learning rate of :arg2 expecting the Output to be InputA NAND InputB
     */
    public function iTrainForTimesWithALearningRateOfExpectingTheOutputToBeInputaNandInputb($runs, $learningRate)
    {
        $this->network->learningRate($learningRate);
        $this->times = $runs;
        $this->learningRate = $learningRate;
        for ($run = 1; $run <= $this->times; $run++) {

            $a = rand(0, 1);
            $b = rand(0, 1);

            $expected = (int) ! ((bool)$a && (bool)$b);

//            echo "TRAINING RUN $run with $a AND $b --> $expected\n";

            if (! (bool)$a) {$a = self::APPX_ZERO;} else $a = self::APPX_ONE;
            if (! (bool)$b) {$b = self::APPX_ZERO;} else $b = self::APPX_ONE;
            if (! (bool)$expected) {$expected = self::APPX_ZERO;} else $expected = self::APPX_ONE;
            
            
            $this->input[0]->set((float) $a);
            $this->input[1]->set((float) $b);

            $this->network->feedForward();

            $target = (float) $expected;
            $this->network->backPropagate([$target]);
        }

    }





























    /**
     * @When I train for :arg1 times with a learning rate of :arg2 expecting the Output to always be :arg3
     */
    public function iTrainForTimesWithALearningRateOfExpectingTheOutputToAlwaysBe($runs, $learningRate, $expected)
    {
        $this->network->learningRate($learningRate);
        $this->times = $runs;
        $this->learningRate = $learningRate;
        for ($run = 1; $run <= $this->times; $run++) {

//            echo "TRAINING RUN $run with 1 AND 1 --> $expected\n";

            $this->input[0]->set((float) 1.0);
            $this->input[1]->set((float) 1.0);

            $this->network->feedForward();

            $target = (float) $expected;
            $this->network->backPropagate([$target]);
        }
    }

    /**
     * @When I train for :arg1 times with a learning rate of :arg2 expecting an input of :arg3 to give an Output of :arg4
     */
    public function iTrainForTimesWithALearningRateOfExpectingAnInputOfToGiveAnOutputOf($runs, $learningRate, $input, $expected)
    {
        $this->network->learningRate($learningRate);
        $this->times = $runs;
        $this->learningRate = $learningRate;
        for ($run = 1; $run <= $this->times; $run++) {

//            echo "TRAINING RUN $run with $input --> $expected\n";

            $this->input[0]->set((float) $input);

            $this->network->feedForward();

//            echo ">>>FF>>>\n";
            $i = $this->input[0]->activation();
            $h = $this->hidden[0]->activation();
            $o = $this->output[0]->activation();
            $wIn = $this->toIn[0]->weight();
            $wHidden = $this->toHidden[0]->weight();
//            echo "$i --> [$wIn] --> $h --> [$wHidden] --> $o\n";

            $target = (float) $expected;
            $this->network->backPropagate([$target]);

//            echo "<<<BP<<<\n";

        }
    }

    /**
     * @Then I want these inputs to give outputs within :arg1% of these
     */
    public function iWantTheseInputsToGiveOutputsWithinOfThese($arg1, TableNode $table)
    {
        foreach ($table->getHash() as $row) {

            $a = $row['Input A'];
            $b = $row['Input B'];
            $o = $row['Output'];

            $this->input[0]->set($a);
            $this->input[1]->set($b);
            $this->network->feedForward();

//            PHPUnit_Framework_Assert::assertEquals((float) $o, (float) $this->output[0]->activation(), "Not Logical", 0.1);
            echo "OUTPUT {$this->output[0]->activation()}\n";
        }
    }

    private function randomWeight()
    {
        $w = (float) (rand(0, 10) - 5) / 10 ;
//        echo "Random Weight: $w\n";

        return $w;
    }







    /**
     * @Then I want this input to give this output within :arg1% accuracy
     */
    public function iWantThisInputToGiveThisOutputWithinAccuracy($arg1, TableNode $table)
    {
        foreach ($table->getHash() as $row) {

            $a = $row['Input'];
            $o = $row['Output'];

            $this->input[0]->set($a);
            $this->network->feedForward();

//            PHPUnit_Framework_Assert::assertEquals((float) $o, (float) $this->output[0]->activation(), "Not Logical", 0.1);
            echo "OUTPUT {$this->output[0]->activation()}\n";
        }
    }
}
