<?php

use ANN\Connection;
use ANN\InputNeuron;
use ANN\Network;
use ANN\Neuron;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    /** @var Neuron */
    private $neuron;

    /** @var float */
    private $target;

    /** @var Connection[] */
    private $connections;

    /** @var int */
    private $trainedFor;

    /** @var float */
    private $learningRate;

    /** @var InputNeuron[] */
    private $input;

    /** @var Neuron[] */
    private $hidden;

    /** @var Neuron[] */
    private $output;

    /** @var int */
    private $batchSize;

    /** @var Network */
    private $ann;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        srand();
    }

    /**
     * @Given I am an output neuron
     */
    public function iAmAnOutputNeuron()
    {
        $this->neuron = new Neuron([]);
    }

    /**
     * @Given I am connected to input neurons with activations and weights
     */
    public function iAmConnectedToInputNeuronsWithActivationsAndWeights(TableNode $table)
    {
        $this->connections = [];
        foreach ($table->getHash() as $row) {
            $this->connections[] = new Connection(new InputNeuron($row['activation']), $row['weight']);
        }
        $this->neuron->connect($this->connections);
    }

    /**
     * @Given I am connected to hidden neurons with activations and weights
     */
    public function iAmConnectedToHiddenNeuronsWithActivationsAndWeights(TableNode $table)
    {
        $this->connections = [];
        foreach ($table->getHash() as $row) {
            $neuron = new Neuron([]);
            $neuron->set($row['activation']);
            $this->connections[] = new Connection($neuron, $row['weight']);
        }
        $this->neuron->connect($this->connections);
    }

    /**
     * @Given I am connected to an input neuron with activation :arg1 and weight :arg2
     */
    public function iAmConnectedToAnInputNeuronWithActivationAndWeight($activation, $weight)
    {
        $input             = new InputNeuron($activation);
        $connection        = new Connection($input, $weight);
        $this->connections = [$connection];

        $this->neuron->connect($this->connections);
    }

    /**
     * @When I feed forward
     */
    public function iFeedForward()
    {
        $this->neuron->feedForward();
    }

    /**
     * @Then my sum should be :arg1
     */
    public function mySumShouldBe(float $expected)
    {
        PHPUnit_Framework_Assert::assertSame($expected, $this->neuron->sum());
    }

    /**
     * @Then my out should be the sigmoid of the sum
     */
    public function myOutShouldBeTheSigmoidOfTheSum()
    {
        $sigmoid = 1 / (1 + exp(- $this->neuron->sum()));
        
        PHPUnit_Framework_Assert::assertSame($sigmoid, $this->neuron->activation());
    }

    /**
     * @When my target value for out is :arg1
     */
    public function theMyTargetValueForOutIs($target)
    {
        $this->target = $target;
    }

    /**
     * @Then the new weights should be
     */
    public function theNewWeightsShouldBe(TableNode $table)
    {
        foreach ($table->getHash() as $row) {
            $activation          = $row['activation'];
            $oldWeight           = $row['oldweight'];
            $newWeight           = $row['newweight'];
            $foundOriginalNeuron = false;
            foreach ($this->connections as $c) {
                if ($c->out() == $activation) {
                    // calculate the adjustment from first principal (put into table later)
                    $adjustment     = $this->calculateWeightAdjustment(
                        $this->target,
                        $this->neuron->activation(),
                        $c->out(),
                        1
                    );
                    $expectedWeight = $oldWeight - $adjustment;
//                    echo "adjustment for activation $activation is $adjustment\n";
//                    echo "new weight = $expectedWeight\n";
                    PHPUnit_Framework_Assert::assertEquals($expectedWeight, $c->weight(),
                        "new weight not what was calculated");
                    PHPUnit_Framework_Assert::assertEquals($newWeight, $c->weight(),
                        "new weight not what was expected");
                    $foundOriginalNeuron = true;
                }
            }
            if (! $foundOriginalNeuron) {
                PHPUnit_Framework_Assert::assertTrue(false,
                    "Could not find the connection with an activation of $activation");
            }
        }
    }

    /*
     * --- from NeuronSpec, bad i know
     */

    private function calculateWeightAdjustment(float $target, float $output, float $input, $learningRate)
    {
        $grad = $this->gradientFunction($target, $output, $input);

//        return $grad * $grad * $learningRate;
        return $grad * $learningRate;
    }

    /**
     * @param float $target
     * @param float $activation
     * @param Connection $c
     *
     * @return string
     */
    private function gradientFunction(float $target, float $output, float $input) : float
    {
        return ($output - $target) *
               ($output * (1 - $output)) *
               $input;
    }

    /**
     * @When my learningRate is :arg1
     */
    public function myLearningrateIs($rate)
    {
        $this->learningRate = $rate;
    }

    /**
     * @When I backpropagate
     */
    public function iBackpropagate()
    {
        $error = $this->neuron->activation() - $this->target;
        $this->neuron->backPropagate($error, $this->learningRate);
    }

    /**
     * @When I train for :arg1 times with a learning rate of :arg2
     */
    public function iTrainForTimesWithALearningRateOf($times, $rate)
    {
        $this->trainedFor   = $times;
        $this->learningRate = $rate;
        for ($i = 1; $i <= $times; $i ++) {
            $this->neuron->feedForward();
            $error = $this->neuron->activation() - $this->target;
            $this->neuron->backPropagate($error, $rate);
        }
    }

    /**
     * @Then my output should be less then :accuracy
     */
    public function myOutputShouldBeLessThen($accuracy)
    {
        PHPUnit_Framework_Assert::assertTrue($this->neuron->activation() < $accuracy);
    }

    /**
     * @When echo my status
     */
    public function echoMyStatus()
    {
        $msg   = "Training Runs: {$this->trainedFor} with Learning Rate: {$this->learningRate}\n";
        $error = ($this->neuron->activation() - $this->target) * 100;
        echo "$msg\n";
        echo ">> Output = {$this->neuron->activation()}\n";
        echo ">> Weight = {$this->connections[0]->weight()}\n";
        echo ">> Error%  = $error\n\n";
    }



}
