<?php

class Calculator
{
    /**
     * @param float $target
     * @param float $output
     * @param float $input
     * @param $learningRate
     *
     * @return float
     */
    public static function calculateWeightAdjustment(float $target, float $output, float $input, $learningRate)
    {
        $grad = self::gradientFunction($target, $output, $input);

        return $grad * $learningRate;
    }

    /**
     * @param float $target
     * @param float $output
     * @param float $input
     *
     * @return float
     */
    public static function gradientFunction(float $target, float $output, float $input) : float
    {
        return ($output - $target) *
               ($output * (1 - $output)) *
               $input;
    }

}
