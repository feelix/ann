Feature: Check neuron behaviour, no bias, even when running under network context.

  Background:

  Scenario: Neuron should be able to feedforward
    Given I am an output neuron
    And I am connected to input neurons with activations and weights
      | activation | weight |
      | 2          | 0.1    |
      | 3          | 0.2    |
      | 4          | 0.3    |
    When I feed forward
    Then my sum should be 2.0
    And my out should be the sigmoid of the sum

  Scenario: Neuron should be able to back propagate
    Given I am an output neuron
    And I am connected to hidden neurons with activations and weights
      | activation | weight |
      | 2          | 0.1    |
      | 3          | 0.2    |
      | 4          | 0.3    |
    And I feed forward
    When my target value for out is 3
    And my learningRate is 1
    And I backpropagate
    Then the new weights should be
      | activation | oldweight | newweight        |
      | 2          | 0.1       | 0.54500542596138 |
      | 3          | 0.2       | 0.86750813894207 |
      | 4          | 0.3       | 1.1900108519228  |

  Scenario: Simply turn 1 into 0
    Given I am an output neuron
    When I am connected to an input neuron with activation 0.099 and weight 0.5
    And my target value for out is 0
    When I train for 10000 times with a learning rate of 1.0
    Then echo my status

  Scenario: Simply turn 0 into 1
  This takes a LOT longer to learn
    Given I am an output neuron
    When I am connected to an input neuron with activation 0.01 and weight '-4'
    And my target value for out is 1.0
    When I train for 100000 times with a learning rate of 10.0
    Then echo my status


