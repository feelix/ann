Feature: Check network behaviour

  ## make the function specifiable
  ## perform the validation - ie the last Then clause!

  Scenario: ANN should be able to learn the NAND boolean operation, which it does but only
    when given bias neurons
    Given I am a 3 layer ANN with 2 Input, 2 Hidden and 1 Output neurons
    When I train for 20000 times with a learning rate of 1.0 expecting the Output to be InputA NAND InputB
    Then I want these inputs to give outputs within 10% of these
    |Input A|Input B|Output|
    |0.01      |0.01      |0.9     |
    |0.01      |0.9      |0.9     |
    |0.9      |0.01      |0.9     |
    |0.9      |0.9      |0.01     |

  Scenario: ANN should be able to turn two ones into a 1, so lets try
    more hidden layer neurons
    Given I am a 3 layer ANN with 2 Input, 5 Hidden and 1 Output neurons
#    When I train for 10000 times with a learning rate of 1.0 expecting the Output to be InputA NAND InputB
    When I train for 100 times with a learning rate of 1.0 expecting the Output to always be 1.0
    Then I want these inputs to give outputs within 10% of these
    |Input A|Input B|Output|
    |1      |1      |1.0   |

  Scenario: ANN should be able to turn 1 into 0
  more hidden layer neurons BUT maybe it cant!
    Given I am a 3 layer ANN with 1 Input, 1 Hidden and 1 Output neurons
#    When I train for 10000 times with a learning rate of 1.0 expecting the Output to be InputA NAND InputB
    When I train for 10000 times with a learning rate of 0.1 expecting an input of 1.0 to give an Output of 0.1
    Then I want this input to give this output within 10% accuracy
      | Input | Output |
      | 1.0   | 0.1    |


#  Scenario: ANN should be able to learn the XOR boolean operation
#    Given I am a 3 layer ANN with 2 Input, 2 Hidden and 1 Output neurons
#    When I train for 1000 times expecting the Output to be InputA XOR InputB
#    Then I want these inputs to give outputs within 10% of these
#    |Input A|Input B|Output|
#    |0      |0      |0     |
#    |0      |1      |1     |
#    |1      |0      |1     |
#    |1      |1      |0     |


