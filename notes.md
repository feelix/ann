#Random notes and thoughts for things to do with ANN project

##Improvements to 'Hello World' Plaything

* Fitness checker for initial weights settings
* 'Perfect' initial weights from artificially perfectly fit normal gaussian distribution


##Visualisation - done

* table showing weights and activations for all connections and neurons during feed forward.
* table showing adjustments and error during back propagation for all connections and neurons.
* better colours for the back propagation visualisation


##Interactivity

* done allow specification of layer sizes (may need redraw canvas, or delay creation till after)
* allow input neurons to be clicked, causing a feed forward


##Investigation - done

* Why so hard to learn XOR?
   - try changing initial weights -- do some research on this!
   - try changing #hidden neurons
   - try changing accuracy


##Future: What else is worthwhile?

* Half and full adder - just for a laugh
* Steering - guide a thing back to home, needs quick learning rate
  * don't want to do it same way as Nature Of Code (calling weights into a perceptron the new Vx and Vy)
  * want something more generic (outputs are Vx and Vy)
   


##And Beyond...

1) STILL want to do the MNIST dataset 
2) Explore Stochastic vs Batch learning.
